//Declarare constante 

const button = document.getElementById("enter");
const input = document.getElementById("userInput");
const descriptionInput = document.getElementById("userInputDescription");
const ul = document.querySelector("ul");
const dropdown = document.querySelector("select");
const li = ul.getElementsByTagName('li');
const buttonAll = document.getElementById("all");
const buttonLactate = document.getElementById("lactate");
const buttonCarne = document.getElementById("carne");
const buttonSucuri = document.getElementById("sucuri");

//Creare elemente si adaugare la array

let produse = []
let id_produs = 0; //variabila globala care va atribui un id unic fiecarui produs

function createElement(){
    const li = document.createElement("li");
    const span = document.createElement("span");
    const icon = document.createElement("i");
    const buttonDescription = document.createElement("button");
    let produsNou = {};

    icon.classList.add("fas", "fa-trash");
    icon.addEventListener("click", deleteElement);
    span.className = "shopping-item-name";
    span.textContent = input.value;

    produsNou.nume = input.value;
    produsNou.descriere = descriptionInput.value;

    buttonDescription.textContent = "details";
    buttonDescription.className = "detalii";
    buttonDescription.id = `item-${id_produs}`; //atribuim id_produs butonului de descriere din elementul corespunzator
    buttonDescription.addEventListener("click",(e) => showModal(e));

    let sub = dropdown.options[dropdown.selectedIndex].value;
    produsNou.categorie = sub;
    li.classList.add(sub, "all");

    produsNou.id = id_produs; //cream un camp nou de tip id in array unde salvam id-urile 
    id_produs++; //crestem id-ul

    console.log(li);
 
    li.appendChild(span);
    li.appendChild(buttonDescription);
    li.appendChild(icon);
    ul.appendChild(li);

    input.value="";
    descriptionInput.value="";

    produse.push(produsNou);
    console.log(produse);
}

button.addEventListener("click", createElement);


//Stergere elemente

function deleteElement(event){
const detalii = event.target.parentElement.children[1].id.split("-");
//console.log(detalii);
let id = Number(detalii[1]);
 
event.target.parentElement.remove();

let new_produse = []; //refac vectorul de obiecte 
for (let i = 0; i < produse.length; i++){
    if (produse[i].id != id){ //daca produsul nu are acelasi id din array cu id-ul elementului html sters, il adaugam in noul array
        new_produse.push(produse[i]);
    }
}

produse = new_produse; //egalam vechiul array cu noul array

console.log(produse);
}

//Filtrare elemente

function filtrareLactate(){
    let i;
    for(i=0;i<li.length;i++){
        if(li[i].className=="lactate all"){
            li[i].style.display="";
        }
        else{
            li[i].style.display="none";
        }
    }
}

function filtrareAll(){
    let i;
        for (i = 0; i < li.length; i++) {
            if(li[i].className == "sucuri all" || li[i].className == "lactate all" || li[i].className == "carne all")
                li[i].style.display = "";
            else{
                li[i].style.display="none";
            }
        }
}

buttonLactate.addEventListener("click", filtrareLactate);
buttonAll.addEventListener("click", filtrareAll);




//CLAUDIA 
//Ce e o modala? explic ca avem html facut pentru modala si pentru content-grey, descriu html-ul
//Deschidere Modala

const modal = document.querySelector(".modal");
const modalHeader = document.querySelector(".modal-header");
const buttonExit = document.querySelector(".exit");
const contentGrey = document.querySelector(".content-grey");

function showModal(e) {
    e.preventDefault(); //previne comportamentul default al elementelor html
    e.stopImmediatePropagation(); //un comportament al functiei sa nu se propage in copii
    
    const h1 = document.createElement("h1"); //declar h1 variabila de forma unui titlu
    const p = document.createElement("p"); // declar p vairabila de forma unui paragraf

    let info = e.target.id.split("-"); 
    let id = Number(info[1]); //explic ca preluam id-ul elementelor si l convertesc la numar

    let produsCautat = produse.find((x,index)=>{
        return produse[index].id===id;
    }) // identific produsul cautat ????

    //console.log(produsCautat);
    h1.textContent = produsCautat.nume; //copiem valoarea in titlul creat 
    p.textContent = produsCautat.descriere; //copiem valoarea in paragraful creat

    modal.appendChild(h1);
    modal.appendChild(p); // le dau append

    modal.style.display = "block"; // modificam display-ul ca block
    contentGrey.style.display = "block"; //modific efectul de grey din background ca block
    }

    //adaug event listener pe butonul de descriere in functia create element, explic ce e =>

//Inchidere Modala
function closeModal(){
    modal.style.display = "none"; //modific astea la loc in none
    contentGrey.style.display = "none";
    modal.innerHTML=""; //sterg continutul vechiului obiect din modala 
    modal.appendChild(modalHeader); // dam iar append la modal header care are doar butonul "X"
    }
    
buttonExit.addEventListener("click", closeModal); // adaug event listener pe "X"

//Dark Mode
const luna = document.querySelector(".fa-moon");
const soare = document.querySelector(".fa-sun");
const body = document.querySelector("body");

soare.classList.toggle("off-sun"); //setez dark mode pe light by default, explic ce e toggle

function darkMode() {
    body.classList.toggle("dark-mode"); //dau toggle la clasa dark mode pe body odata cu apasarea functiei
    body.style.transition = "1s"; // tranzitie 
    soare.classList.toggle("off-sun"); // arata in css ce sunt aceste clase off, dau toggle pe clasele respective 
    luna.classList.toggle("off-moon");
 }

 soare.addEventListener("click", darkMode);
 luna.addEventListener("click", darkMode);
 //la final pun onclick darkMode pe iconite